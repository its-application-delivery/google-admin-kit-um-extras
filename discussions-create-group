#!/usr/bin/python

import re, yaml, umich_gak_utils, json
from json import load
from httplib2 import Http
from oauth2client.client import SignedJwtAssertionCredentials
from apiclient.discovery import build
from apiclient.errors import HttpError
   
def main():

  # Collect data about the new discussion group
  name = raw_input('What name should we use for this discussion group?: ')
  email = raw_input('What email address should the group use? (by default, the concatenated name of the group will be used): ')
  owners = raw_input('List the full email address(es) of any owner(s) here, separated by commas: ')
  members = raw_input('List the full email address(es) of any member(s) here, separated by commas: ')
  description = raw_input('What description should we use for the group?: ')

  # Load the config
  config = {}
  with open('/etc/google-admin-kit.yml') as stream:
    configFile = yaml.load(stream)['discussions']
  config.update(configFile['default'])
  if 'discussions-create-group' in configFile.keys() and configFile['discussions-create-group'] is not None:
    config.update(config['discussions-create-group'])

  # Kick up a logger, because pretty pretty output.
  logger = umich_gak_utils.logger(config)

  # Merge the credentials file into config
  credentials = json.load(open(config['credentials_file']))
  config.update(credentials)

  # Create a sane email address automatically if one isn't specified.
  if email == '':
    email = "{email}@{domain}".format(email=name.replace(' ',''), domain=config['domain'])
  elif '@' not in email:
    email = "{email}@{domain}".format(email=email, domain=config['domain'])

  # Clean email address using magic regex. (removes spaces, apostrophes, and quotes)
  email = re.sub("[\s'\"]", '', email.strip())

  # Also, let's make sure that we've got full email addresses to work with.
  owners = map(lambda owner: owner.strip() + '@' + config['domain'] if '@' not in owner else owner.strip(), owners.split(','))
  if members: 
    members = map(lambda member: member.strip() + '@' + config['domain'] if '@' not in member else member.strip(), members.split(','))
 
  # Cook up some API credentials. 
  credentials = SignedJwtAssertionCredentials(
    config['client_email'],
    config['private_key'],
    'https://www.googleapis.com/auth/admin.directory.group',
    sub=config['admin_account']
  )

  # Create an HTTP object to use, then feed it our credentials.
  http = Http()
  credentials.authorize(http)

  # Now that we're ready, build the first call to create the new group.
  service = build('admin', 'directory_v1', http=http)
  try:
    group_results = service.groups().insert(
      body={
        "email":email,
        "kind":"admin#directory#group",
        "name":name,
        "description":description,
      }
    ).execute()
    logger.log("g_entity={group},message='Group created'".format(group=name), 'DEBUG')
  except HttpError as err:
    logger.log("g_entity={group},message='Unable to create group.'".format(group=name), 'ERR')
    logger.log("g_entity={group},message='{err}'".format(group=name, err=err), 'ERR')
    exit(2)
  
  # If we get an ID value back from the group creation, start populating owners and members.
  if group_results['id']:
    for owner in owners:
      try:
        insert_results = service.members().insert(
          groupKey=group_results['id'],
          body={
            'kind':'admin#directory#member',
            'email':owner,
            'role':'OWNER',
            'type':'USER',
          }
        ).execute()
        logger.log("g_entity={group},message='Added {owner} as group owner'".format(group=name, owner=owner), 'DEBUG')
      except HttpError as err:
        logger.log("g_entity={group}, message='Unable to add {owner} as group owner. Please add manually.'".format(group=name, owner=owner), 'WARNING')
        logger.log("g_entity={group}, message='{err}'".format(group=name, err=err), 'WARNING')

    if members:
      for member in members:
        try:
          insert_results = service.members().insert(
            groupKey=group_results['id'],
            body={
              'kind':'admin#directory#member',
              'email':member,
              'role':'MEMBER',
              'type':'USER',
            }
          ).execute()
          logger.log("g_entity={group},message='Added {member} as group member'".format(group=name, member=member), 'DEBUG')
        except HttpError as err:
          logger.log("g_entity={group},message='Unable to add {member} as group member. Please add manually.'".format(group=name, member=member), 'WARNING')
          logger.log("g_entity={group},message='{err}'".format(group=name, err=err), 'WARNING')

    # Finally, make sure that members outside the discussions.umich.edu domain can be added.
    credentials = SignedJwtAssertionCredentials(
      config['client_email'],
      config['private_key'],
      'https://www.googleapis.com/auth/apps.groups.settings',
      sub=config['admin_account']
    )

    # Create an HTTP object to use, then feed it our credentials.
    http = Http()
    credentials.authorize(http)

    service = build('groupssettings', 'v1', http=http)
    
    try:
      service.groups().update(
        groupUniqueId=group_results['email'],
        body={
          "allowExternalMembers":'true',
          "isArchived":'true'
        }
      ).execute()
      logger.log("g_entity={group},message='Group set to allow external members and archive messages.'".format(group=name), 'DEBUG')
    except HttpError as err:
      logger.log("g_entity={group},message='Unable to set group to allow external members. Please do this manually via the group's settings in the admin console.'".format(group=name), 'WARNING')
      logger.log("g_entity={group},message='{err}'".format(group=name, err=err), 'WARNING')

    logger.log("g_entity={group},message='Discussion group has been created.'".format(group=name), 'INFO')
    # Finally, print some canned text to send to the user. 
    print('================================================\n') 
    print("Hello!\r\n\nThe new discussion group you requested with the name '{name}' has been created and is ready for use. The email address for this discussion group is '{email}'. Information on managing this group and its members can be found at the following url:\r\n\nhttp://documentation.its.umich.edu/node/578\r\n\nIf you have additional questions or concerns about using this group, please contact 4HELP at 734.764.4357 or 4HELP@umich.edu.\n".format(name=name,email=email.lower()))

if __name__ == '__main__':
      main()

